import { Authentication } from './authentication.model';
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        let currentUser: Authentication = JSON.parse(localStorage.getItem('usuarioLogado'));
        if (currentUser && currentUser.accessToken) {
            if (localStorage.getItem('database')) {
                request = request.clone({
                    setHeaders: {
                        Authorization: `Bearer ${currentUser.accessToken}`,
                        Database: localStorage.getItem('database')
                    }
                });
            } else {
                request = request.clone({
                    setHeaders: {
                        Authorization: `Bearer ${currentUser.accessToken}`,
                    }
                });
            }
        }

        return next.handle(request);
    }
}